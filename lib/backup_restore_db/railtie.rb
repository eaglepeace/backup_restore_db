class BackupRestoreDb::Railtie < Rails::Railtie
    rake_tasks do
      load 'tasks/backup.rake'
    end
  end