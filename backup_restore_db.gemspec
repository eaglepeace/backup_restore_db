require_relative 'lib/backup_restore_db/version'

Gem::Specification.new do |spec|
  spec.name          = "backup_restore_db"
  spec.version       = BackupRestoreDb::VERSION
  spec.authors       = ["Corrado Tuccitto"]
  spec.email         = ["tcorrado82@gmail.com"]

  spec.summary       = %q{backup and restore your db}
  spec.description   = %q{backup and restore your db with a single line}
  spec.homepage      = "https://bitbucket.org/eaglepeace/backup_restore_db/src/master/"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  # spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
